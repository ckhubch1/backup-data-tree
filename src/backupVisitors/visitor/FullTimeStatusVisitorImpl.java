package backupVisitors.visitor;

import backupVisitors.myTree.Node;
import backupVisitors.util.TreeBuilder;
import static backupVisitors.util.TreeBuilder.flag;

public class FullTimeStatusVisitorImpl implements TreeVisitorI
{
	TreeBuilder TBObj = new TreeBuilder();
	
	public void visit(Node node)
	{
		checkPreorder(node);
	}
	
	// Method to check if less than 3 courses exist
	public void checkPreorder(Node node)
	{
		if (node == null)
			return;
		if(node.Courses.size()<3)
		{
			flag = 1;
			TBObj.Insert(node.BNumber,"S");
		}
		//System.out.print(node.Courses + " ");
		checkPreorder(node.left);
		checkPreorder(node.right);
	}
}
