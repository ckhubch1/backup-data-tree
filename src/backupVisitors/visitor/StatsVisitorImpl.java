package backupVisitors.visitor;

import backupVisitors.myTree.Node;
import backupVisitors.util.Results;
import java.util.ArrayList;
import java.util.Collections;

public class StatsVisitorImpl implements TreeVisitorI
{
	Results RObj = new Results();
	public static int totalCourses;
	public static int totalBNo = 0;
	public static int average;
	public ArrayList<Integer> array = new ArrayList<Integer>();
	public static float median;
	
	public void visit(Node node)
	{
		calcStats(node);
		average = totalCourses/totalBNo;		
		Collections.sort(array);
		int size = array.size();
		if(size%2!=0)
		{
			median = (float) array.get(size/2);
		}
		else
		{
			median = (float) (array.get(size/2) + array.get(size/2 - 1))/2;
		}
		RObj.writeStats("stats.txt", average, median);
	}
	
	public void calcStats(Node node)
	{
		if (node == null)
			return;
		totalCourses += node.Courses.size();
		totalBNo++;
		array.add(node.Courses.size());
		calcStats(node.left);
		calcStats(node.right);
	}
}
