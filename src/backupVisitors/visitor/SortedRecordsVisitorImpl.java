package backupVisitors.visitor;

import backupVisitors.myTree.Node;
import backupVisitors.util.Results;


public class SortedRecordsVisitorImpl implements TreeVisitorI
{
	Results RObj = new Results();
	
	public void visit(Node node)
	{
		descending(node);
	}
	
	void descending(Node root)
	{
	    if(root == null) 
	    return;
	    descending(root.right);
	    RObj.writeSorted("sorted.txt", root);
	    descending(root.left);
	}
	
}