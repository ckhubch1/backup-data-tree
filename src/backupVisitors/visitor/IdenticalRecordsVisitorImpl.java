package backupVisitors.visitor;

import static backupVisitors.util.TreeBuilder.Original_Node;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import backupVisitors.myTree.Node;
import backupVisitors.util.Results;

public class IdenticalRecordsVisitorImpl implements TreeVisitorI
{
	Results RObj = new Results();
	public ArrayList<ArrayList<Integer>> Group = new ArrayList<>();
	public ArrayList<Integer> Num = new ArrayList<>();
	public int k = 0;
	public Set<Integer> hs = new HashSet<>();
	public Set<ArrayList<Integer>> as = new HashSet<>();

	public void visit(Node node)
	{
		identicalCheck(node);
	}

	public void identicalCheck(Node node)
	{
		for (int i = 0; i < Original_Node.size(); i++)
		{
			for (int j = 0; j < Original_Node.size(); j++)
			{
				if (Original_Node.get(i).Courses.equals(Original_Node.get(j).Courses))
				{
					Num.add(Original_Node.get(i).BNumber);
					Num.add(Original_Node.get(j).BNumber);
				}
			}
			Group.add(Num);
			Num = create();
		}

		for (int i = 0; i < Group.size(); i++)
		{
			hs.addAll(Group.get(i));
			Group.get(i).clear();
			Group.get(i).addAll(hs);
			hs.clear();
			Collections.sort(Group.get(i));
		}
		
		as.addAll(Group);
		Group.clear();
		Group.addAll(as);
		as.clear();

		RObj.writeIdentical(Group,"identical.txt");	
	}

	public ArrayList<Integer> create()
	{
		ArrayList<Integer> Num2 = new ArrayList<>();
		return Num2;
	}
}
