package backupVisitors.visitor;

import backupVisitors.myTree.Node;

public interface TreeVisitorI
{
	public void visit(Node node);
}
