package backupVisitors.util;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import backupVisitors.myTree.Node;

public class Results implements FileDisplayInterface, StdoutDisplayInterface
{
	FileWriter write = null;
	public static int ral = 1;
	public static int a = 1;

	public void printNodes(ArrayList<Node> Original_Node, String fileName)
	{
		writeToScreen(Original_Node);
		writeToFile(Original_Node, fileName);
	}

	public void writeToScreen(ArrayList<Node> Original_Node)
	{
		for (int i = 0; i < Original_Node.size(); i++)
		{
			System.out.print(Original_Node.get(i).BNumber + " -> ");
			for (int j = 0; j < Original_Node.get(i).Courses.size(); j++)

			{
				System.out.print(Original_Node.get(i).Courses.get(j) + " ");
			}
			System.out.print("\n");
		}
	}

	public void writeStats(String fileName, int average, float median)
	{
		int val = 1;
		try
		{
			if (val == 1)
			{
				FileWriter clear = new FileWriter(fileName);
				clear.write("");
				clear.close();
				val = 0;
			}

			write = new FileWriter(fileName, true);
			write.write("The Average of Courses registered by each student is: " + average + "\n");
			write.write("The Median of Courses registered by each student is: " + median);
			write.close();
		}

		catch (IOException e)
		{
			System.err.println("Error while writing to output.txt");
		}
	}

	public void writeSorted(String fileName, Node node)
	{
		try
		{
			if (ral == 1)
			{
				FileWriter clear = new FileWriter(fileName);
				clear.write("");
				clear.close();
				ral = 0;
			}

			write = new FileWriter(fileName, true);
			if (a == 1)
			{
				write.write("Contents of Backup2 Node in Descending Order\n");
				a = 0;
			}
			write.write("" + node.BNumber);
			write.write(" -> " + node.Courses + "\n");
			write.close();
		}

		catch (IOException e)
		{
			System.err.println("Error while writing to output.txt");
		}
	}

	public void writeIdentical(ArrayList<ArrayList<Integer>> node, String fileName)
	{
		int val = 1;
		try
		{
			if (val == 1)
			{
				FileWriter clear = new FileWriter(fileName);
				clear.write("");
				clear.close();
				val = 0;
			}

			write = new FileWriter(fileName, true);
			for (int i = 0; i < node.size(); i++)
			{
				if(node.get(i).size() > 1)
				write.write("Group: ");
				for (int j = 0; j < node.get(i).size(); j++)
				{
					if (node.get(i).size() > 1)
					{						
						write.write(""+node.get(i).get(j)+" ");
					}
				}
				if (i + 1 != node.size() && node.get(i).size() > 1)
				{
					write.write("\n");
				}
			}
			write.close();
		}

		catch (IOException e)
		{
			System.err.println("Error while writing to output.txt");
		}
	}

	public void writeToFile(ArrayList<Node> Original_Node, String fileName)
	{
		int val = 1;
		try
		{
			if (val == 1)
			{
				FileWriter clear = new FileWriter(fileName);
				clear.write("");
				clear.close();
				val = 0;
			}

			write = new FileWriter(fileName, true);
			for (int i = 0; i < Original_Node.size(); i++)
			{
				write.write(Original_Node.get(i).BNumber + " -> ");
				for (int j = 0; j < Original_Node.get(i).Courses.size(); j++)
				{
					write.write(Original_Node.get(i).Courses.get(j) + " ");
				}
				if (i + 1 != Original_Node.size())
				{
					write.write("\n");
				}
			}
			write.close();
		}

		catch (IOException e)
		{
			System.err.println("Error while writing to output.txt");
		}

	}
}
