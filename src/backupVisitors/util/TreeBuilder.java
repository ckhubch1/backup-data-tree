package backupVisitors.util;

import java.util.ArrayList;
import java.util.Collections;

import backupVisitors.myTree.Node;

public class TreeBuilder
{
	public static Node root;
	public static Node orig_node;
	public Node curr_node;
	public static ArrayList<Node> Original_Node = new ArrayList<Node>();
	public ArrayList<Node> Backup1_Node = new ArrayList<Node>();
	public ArrayList<Node> Backup2_Node = new ArrayList<Node>();
	public static int flag = 0;

	/*
	 * Binary Search Tree Implementation adapted from URL:
	 * http://algorithms.tutorialhorizon.com/binary-search-tree-complete-
	 * implementation/
	 */

	public void Insert(int BNumberIn, String CourseIn)
	{
		if (flag == 0 && (CourseIn.equals("L") || CourseIn.equals("M") || CourseIn.equals("N") || CourseIn.equals("O")
				|| CourseIn.equals("P") || CourseIn.equals("Q") || CourseIn.equals("R") || CourseIn.equals("S")
				|| CourseIn.equals("T") || CourseIn.equals("U") || CourseIn.equals("V") || CourseIn.equals("W")
				|| CourseIn.equals("X") || CourseIn.equals("Y") || CourseIn.equals("Z")))
		{
			// Ignore such an input case
			return;
		} else
		{
			if (search(BNumberIn) != null)
			{
				curr_node = search(BNumberIn);
				for (int i = 0; i < curr_node.Courses.size(); i++)
				{
					if (curr_node.Courses.get(i).equals(CourseIn))
					{
						// Do nothing as input exists
						return;
					}
				}
				curr_node.Courses.add(CourseIn);
				Collections.sort(curr_node.Courses);
				curr_node.notifyObservers(curr_node);
				return;
			} else
			{
				orig_node = new Node(BNumberIn, CourseIn);
				orig_node.registerObserver(orig_node);
				orig_node.registerObserver(orig_node);

				if (root == null)
				{
					root = orig_node;
					root.backup1 = orig_node;
					root.backup2 = orig_node;
					Original_Node.add(orig_node);
					Backup1_Node.add(orig_node);
					Backup2_Node.add(orig_node);
					return;
				}
				Node current = root;
				Node parent = null;
				while (true)
				{
					parent = current;
					if (BNumberIn < current.BNumber)
					{
						current = current.left;
						if (current == null)
						{
							parent.left = orig_node;
							parent.backup1.left = orig_node;
							parent.backup2.left = orig_node;
							Original_Node.add(orig_node);
							Backup1_Node.add(orig_node);
							Backup2_Node.add(orig_node);
							return;
						}
					} else
					{
						current = current.right;
						if (current == null)
						{
							parent.right = orig_node;
							parent.backup1.right = orig_node;
							parent.backup2.right = orig_node;
							Original_Node.add(orig_node);
							Backup1_Node.add(orig_node);
							Backup2_Node.add(orig_node);
							return;
						}
					}
				}
			}
		}
	}

	public Node search(int BNumberIn)
	{
		Node current = root;
		while (current != null)
		{
			if (current.BNumber == BNumberIn)
			{
				return current;
			} else if (current.BNumber > BNumberIn)
			{
				current = current.left;
			} else
			{
				current = current.right;
			}
		}
		return null;
	}

	public void delete(int BNumberIn, String CourseIn)
	{
		if (search(BNumberIn) != null)
		{
			curr_node = search(BNumberIn);
			for (int i = 0; i < curr_node.Courses.size(); i++)
			{
				if (curr_node.Courses.get(i).equals(CourseIn))
				{
					curr_node.Courses.remove(CourseIn);
					return;
				}
			}
			curr_node.notifyObservers(curr_node);
			return;
		} else
		{
			// Node not found
			return;
		}
	}
	
	public ArrayList<Node> getX()
	{
		return Original_Node;
	}

}
