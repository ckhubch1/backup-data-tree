package backupVisitors.util;

import java.util.ArrayList;

import backupVisitors.myTree.Node;

public interface StdoutDisplayInterface
{
	public void writeToScreen(ArrayList<Node> Original_Node);
}
