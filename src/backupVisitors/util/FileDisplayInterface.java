package backupVisitors.util;

import java.util.ArrayList;

import backupVisitors.myTree.Node;

public interface FileDisplayInterface
{
	public void writeToFile(ArrayList<Node> Original_Node, String fileName);
}
