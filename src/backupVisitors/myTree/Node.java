package backupVisitors.myTree;

import java.util.ArrayList;

import backupVisitors.visitor.TreeVisitorI;

public class Node implements Cloneable, SubjectI, ObserverI
{
	public int BNumber;
	public ArrayList<String> Courses = new ArrayList<String>();
	public Node left;
	public Node right;
	public Node backup1;
	public Node backup2;

	@Override
	public Node clone()
	{
		Node node;
		try
		{
			node = (Node) super.clone();
		} catch (CloneNotSupportedException e)
		{
			throw new Error();
		}
		return node;
	}

	public Node(int BNumberIn, String CoursesIn)
	{
		BNumber = BNumberIn;
		Courses.add(CoursesIn);
		left = null;
		right = null;
		backup1 = null;
		backup2 = null;
	}

	public void registerObserver(Node NodeIn)
	{
		NodeIn.backup1 = (Node) NodeIn.clone();
		NodeIn.backup2 = (Node) NodeIn.clone();
	}

	public void removeObserver(Node nodeIn)
	{
		// Code to removeObserver from Observer list
	}

	public void notifyObservers(Node NodeIn)
	{
		NodeIn.update(NodeIn);
	}

	public void update(Node NodeIn)
	{
		NodeIn.backup1.Courses = NodeIn.Courses;
		NodeIn.backup2.Courses = NodeIn.Courses;
	}
	
	public void accept(TreeVisitorI visitor)
	{
		visitor.visit(this);
	}
	
}
