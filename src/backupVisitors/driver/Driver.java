package backupVisitors.driver;

import backupVisitors.util.FileProcessor;
import backupVisitors.util.Results;
import backupVisitors.util.TreeBuilder;
import backupVisitors.visitor.FullTimeStatusVisitorImpl;
import backupVisitors.visitor.IdenticalRecordsVisitorImpl;
import backupVisitors.visitor.SortedRecordsVisitorImpl;
import backupVisitors.visitor.StatsVisitorImpl;
import backupVisitors.visitor.TreeVisitorI;
import static backupVisitors.util.TreeBuilder.root;

public class Driver
{
	private static int BNumber;
	private static String Course = null;

	public static void main(String[] args)
	{
		// if (args.length != 5)
		// {
		// System.err.println("Error: Check command lines argument");
		// System.exit(0);
		// }
		String line;
		String line2;
		FileProcessor FPObj = new FileProcessor();
		TreeBuilder TBObj = new TreeBuilder();
		Results RObj = new Results();

		// Visitor instantiation
		TreeVisitorI FullTimeStatusV = new FullTimeStatusVisitorImpl();
		TreeVisitorI IdenticalRecordsV = new IdenticalRecordsVisitorImpl();
		TreeVisitorI SortedRecordsV = new SortedRecordsVisitorImpl();
		TreeVisitorI StatsV = new StatsVisitorImpl();

		// File Read for Input
		FPObj.OpenFile("input.txt");
		int count = FPObj.CountLines();
		for (int i = 0; i < count; i++)
		{
			line = FPObj.readLine();
			BNumber = Integer.parseInt(line.substring(0, 4));
			Course = line.substring(5, 6);
			TBObj.Insert(BNumber, Course);
		}
		FPObj.CloseFile();

		// File Read for Delete
		FPObj.OpenFile("delete.txt");
		int count2 = FPObj.CountLines();
		for (int i = 0; i < count2; i++)
		{
			line2 = FPObj.readLine();
			BNumber = Integer.parseInt(line2.substring(0, 4));
			Course = line2.substring(5, 6);
			TBObj.delete(BNumber, Course);
			FPObj.CloseFile();
		}
		// Visitor methods
		root.backup1.accept(StatsV);
		root.accept(FullTimeStatusV);
		root.backup2.accept(SortedRecordsV);
		root.accept(IdenticalRecordsV);

		System.out.println("Contents of Original Node\n");
		RObj.printNodes(TreeBuilder.Original_Node, "output1.txt");
		System.out.println("\nContents of Backup1 Node\n");
		RObj.printNodes(TBObj.Backup1_Node, "output2.txt");
		System.out.println("\nContents of Backup2 Node\n");
		RObj.printNodes(TBObj.Backup2_Node, "output3.txt");

	}

}
